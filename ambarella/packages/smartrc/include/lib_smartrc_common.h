/*******************************************************************************
 * lib_smartrc_common.h
 *
 * History:
 *   2015/11/23 - [ypxu] created file
 *
 * Copyright (c) 2015 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

#ifndef _LIB_SMARTRC_COMMON_H_
#define _LIB_SMARTRC_COMMON_H_

#include <basetypes.h>
#include <config.h>

#ifdef CONFIG_ARCH_S2L
#define QP_MATRIX_SINGLE_SIZE	(IAV_MEM_QPM_SIZE)
#elif defined CONFIG_ARCH_S3L
#define QP_MATRIX_SINGLE_SIZE	(IAV_MEM_ROI_MATRIX_SIZE)
#else
//reserved
#endif
#define SMARTRC_MAX_STREAM_NUM		(IAV_STREAM_MAX_NUM_ALL)
#define SMARTRC_ENCODE_WIDTH_MIN	(160)
#define SMARTRC_ENCODE_WIDTH_MAX	(4096)
#define SMARTRC_ENCODE_HEIGHT_MIN	(90)
#define SMARTRC_ENCODE_HEIGHT_MAX	(3008)

#ifndef AM_IOCTL
#define AM_IOCTL(_flip, _cmd, _arg)	\
	do {	\
		if (ioctl(_flip, _cmd, _arg) < 0) {	\
			perror(#_cmd);	\
			return -1;	\
		}	\
	} while(0);
#endif

#ifndef VERIFY_STREAM_ID
#define VERIFY_STREAM_ID(x) \
	do {	\
		if ((x) < 0 || (x) >= SMARTRC_MAX_STREAM_NUM) {	\
			printf("stream id out of range\n");	\
			return -1;	\
		}	\
	} while(0);
#endif

#ifndef VERIFY_ENCODE_RESOLUTION
#define VERIFY_ENCODE_RESOLUTION(w, h) \
	do {	\
		if ((w) < SMARTRC_ENCODE_WIDTH_MIN ||	\
			(w) > SMARTRC_ENCODE_WIDTH_MAX ||	\
			(h) < SMARTRC_ENCODE_HEIGHT_MIN ||	\
			(h) > SMARTRC_ENCODE_HEIGHT_MAX) {	\
			printf("encode resolution invalid\n");	\
			return -1;	\
		}	\
	} while (0);
#endif

#ifndef ROUND_UP
#define ROUND_UP(size, align) (((size) + ((align) - 1)) & ~((align) - 1))
#endif
#ifndef ROUND_DOWN
#define ROUND_DOWN(size, align) ((size) & ~((align) - 1))
#endif

extern int g_smartrc_log_level;

typedef enum motion_level_e
{
	MOTION_NONE	= 0,
	MOTION_LOW 	= 1,
	MOTION_MID	= 2,
	MOTION_HIGH = 3, //worst case is default
	MOTION_NUM,
	MOTION_FIRST = MOTION_NONE,
	MOTION_LAST = MOTION_NUM,
} motion_level_t;

typedef enum noise_level_e
{
	NOISE_NONE	= 0,
	NOISE_LOW 	= 1,
	NOISE_HIGH	= 2,//worst case is default
	NOISE_NUM,
	NOISE_FIRST = NOISE_NONE,
	NOISE_LAST = NOISE_NUM,
} noise_level_t;

typedef enum quality_level_e
{
	QUALITY_LOW = 0,	//default setting, bitrate ceiling 1Mbps for 1080p
	QUALITY_MEDIUM = 1,	//bitrate ceiling 2Mbps for 1080p
	QUALITY_HIGH = 2,	//bitrate ceiling 4Mbps for 1080p
	QUALITY_ULTIMATE = 3,	//bitrate ceiling 6Mbps for 1080p
	QUALITY_NUM,
	QUALITY_FIRST = QUALITY_LOW,
	QUALITY_LAST = QUALITY_NUM,
} quality_level_t;

typedef enum style_e
{
	STYLE_FPS_KEEP_BITRATE_AUTO = 0, //try to keep max fps, bitrate auto drop when possible (default, consumer style)
	STYLE_QUALITY_KEEP_FPS_AUTO_DROP = 1, //similar to default, but fps can auto drop when no budget to keep quality(Quality keeping)
	STYLE_FPS_KEEP_CBR_ALIKE = 2, //FPS keep, CBR alike. (traditional professional IPCAM style)
	STYLE_NUM,
	STYLE_FIRST = STYLE_FPS_KEEP_BITRATE_AUTO,
	STYLE_LAST = STYLE_NUM,
} style_t;

typedef enum profile_e
{
	PROFILE_STATIC 						= 0,
	PROFILE_SMALL_MOTION 				= 1,
	PROFILE_MID_MOTION					= 2,
	PROFILE_BIG_MOTION					= 3,
	PROFILE_LOW_LIGHT					= 4,
	PROFILE_BIG_MOTION_WITH_FRAME_DROP	= 5,
	PROFILE_SECURITY_IPCAM_CBR 			= 6,
	PROFILE_NUM,
	PROFILE_FIRST = PROFILE_STATIC,
	PROFILE_LAST = PROFILE_NUM,
} profile_t;

typedef struct version_s
{
	int major;
	int minor;
	int patch;
	unsigned int mod_time;
	char description[64];
} version_t;

typedef struct threshold_s
{
	u32 motion_low;
	u32 motion_mid;
	u32 motion_high;
	u16 noise_low;
	u16 noise_high;
} threshold_t;

typedef struct delay_s
{
	u32 motion_indicator;
	u16 motion_none;
	u16 motion_low;
	u16 motion_mid;
	u16 motion_high;
	u16 noise_none;
	u16 noise_low;
	u16 noise_high;
	u16 reserved;
} delay_t;

typedef struct encode_config_s
{
	u32 src_buf_id;
	u32 codec_type;
	u32 fps;
	u32 pitch;
	u32 width;
	u32 height;
	u32 x;
	u32 y;
	u32 M;
	u32 N;
	u32 roi_width;
	u32 roi_height;
} encode_config_t;

typedef struct init_s
{
	int fd_iav;
	int fd_vin;
} init_t;

typedef struct param_config_s
{
	encode_config_t enc_cfg;
	u32 stream_fps;
	u32 stream_id;
	u32 bitrate_gap_adj;
} param_config_t;

typedef struct roi_session_s
{
	u32 dsp_pts;
	u32 count;
	int *tmpdata;
	int *motion_matrix;
	int *prev1;
	int *prev2;
} roi_session_t;

#ifndef VERIFY_MOTION_LEVEL
#define VERIFY_MOTION_LEVEL(x)	\
	do {	\
		if ((x) < MOTION_FIRST || (x) >= MOTION_LAST) {	\
			printf("invalid motion level\n");	\
			return -1;	\
		}	\
	} while (0);
#endif

#ifndef VERIFY_NOISE_LEVEL
#define VERIFY_NOISE_LEVEL(x) \
	do {	\
		if ((x) < NOISE_FIRST || (x) >= NOISE_LAST) {	\
			printf("invalid noise level\n");	\
			return -1;	\
		}	\
	} while (0);
#endif

#ifndef VERIFY_QUALITY_LEVEL
#define VERIFY_QUALITY_LEVEL(x) \
	do {	\
		if ((x) < QUALITY_FIRST || (x) >= QUALITY_LAST) {	\
			printf("invalid quality level\n");	\
			return -1;	\
		}	\
	} while (0);
#endif

#ifndef VERIFY_STYLE
#define VERIFY_STYLE(x)	\
	do {	\
		if ((x) < STYLE_FIRST || (x) >= STYLE_LAST) {	\
			printf("invalid style\n");	\
			return -1;	\
		}	\
	} while (0);
#endif

#ifndef VERIFY_PROFILE
#define VERIFY_PROFILE(x)	\
	do {	\
		if ((x) < PROFILE_FIRST || (x) >= PROFILE_LAST) {	\
			printf("invalid profile\n");	\
			return -1;	\
		}	\
	} while (0);
#endif

#endif /* _LIB_SMARTRC_COMMON_H_ */
