/**
 * am_native.h
 *
 * History:
 *  2015/08/07 - [Zhi He] create file
 *
 * Copyright (c) 2015 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __AM_NATIVE_H__
#define __AM_NATIVE_H__

//common include

#include "stdlib.h"
#include "stdio.h"
#include "string.h"

#include "am_playback_new_if.h"

//-----------------------------------------------------------------------
//
//  Basic types
//
//-----------------------------------------------------------------------

typedef int TInt;
typedef unsigned int TUint;

typedef unsigned char TU8;
typedef unsigned short TU16;
typedef unsigned int TU32;

typedef signed char TS8;
typedef signed short TS16;
typedef signed int TS32;

typedef signed long long TS64;
typedef unsigned long long TU64;

typedef long TLong;
typedef unsigned long TULong;

typedef TS64  TTime;
typedef char TChar;
typedef unsigned char TUChar;

typedef TS64 TFileSize;
typedef TS64 TIOSize;

typedef volatile int TAtomic;

typedef TULong TMemSize;
typedef TULong TPointer;
typedef TULong TFlag;

typedef TInt TSocketHandler;
typedef TU16 TSocketPort;
typedef TInt TSocketSize;

typedef TU16 TComponentIndex;
typedef TU8 TComponentType;

#define DInvalidSocketHandler (-1)
#define DIsSocketHandlerValid(x) (0 <= x)

#define DLEFOURCC(a, b, c, d) (((unsigned int) a) | ((unsigned int) (b << 8)) | ((unsigned int) (c << 16)) | ((unsigned int) (d << 24)))
#define DBEFOURCC(a, b, c, d) (((unsigned int) a << 24) | ((unsigned int) (b << 16)) | ((unsigned int) (c << 8)) | ((unsigned int) d))
#define DUSE_LITTLE_ENDIAN
#ifdef DUSE_LITTLE_ENDIAN
#define DFOURCC DLEFOURCC
#else
#define DFOURCC DBEFOURCC
#endif

#define DLikely(x)   __builtin_expect(!!(x),1)
#define DUnlikely(x)   __builtin_expect(!!(x),0)

#define DMaxStreamNumber  16

enum TimeUnit {
  TimeUnitDen_90khz = 90000,
  TimeUnitDen_27mhz = 27000000,
  TimeUnitDen_ms = 1000,
  TimeUnitDen_us = 1000000,
  TimeUnitDen_ns = 1000000000,
  TimeUnitNum_fps29dot97 = 3003,
};

enum VideoFrameRate {
  VideoFrameRate_MAX = 1024,
  VideoFrameRate_29dot97 = VideoFrameRate_MAX + 1,
  VideoFrameRate_59dot94,
  VideoFrameRate_23dot96,
  VideoFrameRate_240,
  VideoFrameRate_60,
  VideoFrameRate_30,
  VideoFrameRate_24,
  VideoFrameRate_15,
};

enum StreamTransportType {
  StreamTransportType_Invalid = 0,
  StreamTransportType_RTP,
};

enum ProtocolType {
  ProtocolType_Invalid = 0,
  ProtocolType_UDP,
  ProtocolType_TCP,
};

enum StreamingServerType {
  StreamingServerType_Invalid = 0,
  StreamingServerType_RTSP,
  StreamingServerType_RTMP,
  StreamingServerType_HTTP,
};

enum StreamingServerMode {
  StreamingServerMode_MulticastSetAddr = 0, //live streamming/boardcast
  StreamingServerMode_Unicast, //vod
  StreamingServerMode_MultiCastPickAddr, //join conference
};

enum ContainerType {
  ContainerType_Invalid = 0,
  ContainerType_AUTO = 1,
  ContainerType_MP4,
  ContainerType_3GP,
  ContainerType_TS,
  ContainerType_MOV,
  ContainerType_MKV,
  ContainerType_AVI,
  ContainerType_AMR,

  ContainerType_TotolNum,
};

enum StreamFormat {
  StreamFormat_Invalid = 0,
  StreamFormat_H264 = 0x01,
  StreamFormat_VC1 = 0x02,
  StreamFormat_MPEG4 = 0x03,
  StreamFormat_WMV3 = 0x04,
  StreamFormat_MPEG12 = 0x05,
  StreamFormat_HybridMPEG4 = 0x06,
  StreamFormat_HybridRV40 = 0x07,
  StreamFormat_VideoSW = 0x08,
  StreamFormat_JPEG = 0x09,
  StreamFormat_AAC = 0x0a,
  StreamFormat_MPEG12Audio = 0x0b,
  StreamFormat_MP2 = 0x0c,
  StreamFormat_MP3 = 0x0d,
  StreamFormat_AC3 = 0x0e,
  StreamFormat_ADPCM = 0x0f,
  StreamFormat_AMR_NB = 0x10,
  StreamFormat_AMR_WB = 0x11,
  StreamFormat_PCMU = 0x12,
  StreamFormat_PCMA = 0x13,
  StreamFormat_H265 = 0x14,

  StreamFormat_PixelFormat_YUV420p = 0x40,
  StreamFormat_PixelFormat_NV12 = 0x41,
  StreamFormat_PixelFormat_YUYV = 0x42,
  StreamFormat_PixelFormat_YUV422p = 0x43,

  StreamFormat_PCM_S16 = 0x58,

  StreamFormat_FFMpegCustomized = 0x68,
};

enum PixFormat {
  PixFormat_YUV420P = 0,
  PixFormat_NV12,
  PixFormat_RGB565,
};

enum PrivateDataType {
  PrivateDataType_GPSInfo = 0,
  PrivateDataType_SensorInfo,
};

enum EntropyType {
  EntropyType_NOTSet = 0,
  EntropyType_H264_CABAC,
  EntropyType_H264_CAVLC,
};

enum AudioSampleFMT {
  AudioSampleFMT_NONE = -1,
  AudioSampleFMT_U8,          ///< unsigned 8 bits
  AudioSampleFMT_S16,         ///< signed 16 bits
  AudioSampleFMT_S32,         ///< signed 32 bits
  AudioSampleFMT_FLT,         ///< float
  AudioSampleFMT_DBL,         ///< double
  AudioSampleFMT_NB           ///< Number of sample formats. DO NOT USE if linking dynamically
};

enum MuxerSavingFileStrategy {
  MuxerSavingFileStrategy_Invalid = 0x00,
  MuxerSavingFileStrategy_AutoSeparateFile = 0x01, //muxer will auto separate file itself, with more time accuracy
  MuxerSavingFileStrategy_ManuallySeparateFile = 0x02,//engine/app will management the separeting file
  MuxerSavingFileStrategy_ToTalFile = 0x03,//not recommanded, cannot store file like .mp4 if storage have not enough space
};

enum MuxerSavingCondition {
  MuxerSavingCondition_Invalid = 0x00,
  MuxerSavingCondition_InputPTS = 0x01,
  MuxerSavingCondition_CalculatedPTS = 0x02,
  MuxerSavingCondition_FrameCount = 0x03,
};

enum MuxerAutoFileNaming {
  MuxerAutoFileNaming_Invalid = 0x00,
  MuxerAutoFileNaming_ByDateTime = 0x01,
  MuxerAutoFileNaming_ByNumber = 0x02,
};

typedef struct {
  unsigned int pic_width;
  unsigned int pic_height;
  unsigned int pic_offset_x;
  unsigned int pic_offset_y;
  unsigned int framerate_num;
  unsigned int framerate_den;
  float framerate;
  unsigned int M, N, IDRInterval;//control P, I, IDR percentage
  unsigned int sample_aspect_ratio_num;
  unsigned int sample_aspect_ratio_den;
  unsigned int bitrate;
  unsigned int lowdelay;
  EntropyType entropy_type;
} SVideoParams;

typedef struct {
  TGenericID id;
  unsigned int index;
  StreamFormat format;
  unsigned int pic_width;
  unsigned int pic_height;
  unsigned int bitrate;
  unsigned long long bitrate_pts;
  float framerate;

  unsigned char gop_M;
  unsigned char gop_N;
  unsigned char gop_idr_interval;
  unsigned char gop_structure;
  unsigned long long demand_idr_pts;
  unsigned char demand_idr_strategy;
  unsigned char framerate_reduce_factor;
  unsigned char framerate_integer;
  unsigned char reserved0;
} SVideoEncoderParam;

typedef struct {
  unsigned int check_field;
  TGenericID module_id;

  TDimension cap_win_offset_x, cap_win_offset_y;
  TDimension cap_win_width, cap_win_height;
  TDimension screen_width, screen_height;
  TDimension cap_buffer_linesize;

  float framerate;
  unsigned int framerate_num;
  unsigned int framerate_den;

  EPixelFMT pixel_format;
  StreamFormat format;
} SVideoCaptureParams;

typedef struct {
  unsigned int width, height;
  unsigned int framerate_num, framerate_den;

  StreamFormat format;
} SVideoInjectParams;

typedef struct {
  unsigned int sample_rate;
  AudioSampleFMT sample_format;
  unsigned int channel_number;
  unsigned int channel_layout;
  unsigned int frame_size;
  unsigned int bitrate;
  unsigned int need_skip_adts_header;
  unsigned int pts_unit_num;//pts's unit
  unsigned int pts_unit_den;

  unsigned char is_channel_interlave;
  unsigned char is_big_endian;
  unsigned char reserved0, reserved1;

  unsigned int codec_format;
  unsigned int customized_codec_type;
} SAudioParams;

typedef union {
  SVideoParams video;
  SAudioParams audio;
} UFormatSpecific;

#endif

