/*******************************************************************************
 * am_muxer_jpeg_base.cpp
 *
 * History:
 *   2015-10-8 - [ccjing] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/
#include "am_base_include.h"
#include "am_amf_types.h"
#include "am_amf_interface.h"
#include "am_amf_packet.h"
#include "am_log.h"
#include "am_define.h"
#include "am_mutex.h"
#include "am_thread.h"

#include "am_file_sink_if.h"
#include "am_muxer_codec_info.h"
#include "am_muxer_jpeg_file_writer.h"
#include "am_file.h"
#include "am_muxer_jpeg_base.h"

#include <time.h>
#include <unistd.h>
#include <sys/statfs.h>
#include <iostream>
#include <fstream>
#include <ctype.h>

AMMuxerJpegBase::AMMuxerJpegBase() :
    m_thread(nullptr),
    m_state_lock(nullptr),
    m_interface_lock(nullptr),
    m_file_writing_lock(nullptr),
    m_muxer_jpeg_config(nullptr),
    m_config(nullptr),
    m_packet_queue(nullptr),
    m_config_file(nullptr),
    m_file_writer(nullptr),
    m_state(AM_MUXER_CODEC_INIT),
    m_stream_id(0),
    m_run(false),
    m_file_writing(true)
{
}

AM_STATE AMMuxerJpegBase::init(const char* config_file)
{
  AM_STATE ret = AM_STATE_OK;
  do {
    if (AM_UNLIKELY(config_file == nullptr)) {
      ERROR("Config_file is NULL, should input valid config_file");
      ret = AM_STATE_ERROR;
      break;
    }
    m_config_file = amstrdup(config_file);
    if (AM_UNLIKELY((m_packet_queue = new packet_queue()) == nullptr)) {
      ERROR("Failed to create packet_queue in AMMuxerJpegBase.");
      ret = AM_STATE_NO_MEMORY;
      break;
    }
    if (AM_UNLIKELY((m_interface_lock = AMSpinLock::create()) == nullptr)) {
      ERROR("Failed to create interface lock in AMMuxerJpegBase.");
      ret = AM_STATE_NO_MEMORY;
      break;
    }
    if (AM_UNLIKELY((m_state_lock = AMSpinLock::create()) == nullptr)) {
      ERROR("Failed to create m_state_lock in AMMuxerJpegBase.");
      ret = AM_STATE_NO_MEMORY;
      break;
    }
    if (AM_UNLIKELY((m_file_writing_lock = AMSpinLock::create()) == nullptr)) {
      ERROR("Failed to create m_file_writing_lock in AMMuxerJpegBase.");
      ret = AM_STATE_NO_MEMORY;
      break;
    }
    if (AM_UNLIKELY((m_config = new AMMuxerJpegConfig()) == nullptr)) {
      ERROR("Failed to create jpeg config in AMMuxerJpegBase.");
      ret = AM_STATE_NO_MEMORY;
      break;
    }
    m_muxer_jpeg_config = m_config->get_config(std::string(m_config_file));
    if (AM_UNLIKELY(!m_muxer_jpeg_config)) {
      ERROR("Failed to get config in AMMuxerJpegBase");
      ret = AM_STATE_ERROR;
      break;
    }
    m_file_writing = m_muxer_jpeg_config->auto_file_writing;
  } while (0);
  return ret;
}

AMMuxerJpegBase::~AMMuxerJpegBase()
{
  stop();
  delete m_config;
  delete m_packet_queue;
  AM_DESTROY(m_interface_lock);
  AM_DESTROY(m_state_lock);
  AM_DESTROY(m_file_writing_lock);
  delete m_config_file;
  INFO("AMJpegMuxer object destroyed.");
}

AM_STATE AMMuxerJpegBase::start()
{
  AUTO_SPIN_LOCK(m_interface_lock);
  AM_STATE ret = AM_STATE_OK;
  do {
    bool need_break = false;
    switch (get_state()) {
      case AM_MUXER_CODEC_RUNNING: {
        NOTICE("The jpeg muxer is already running.");
        need_break = true;
      }break;
      case AM_MUXER_CODEC_ERROR: {
        NOTICE("Jpeg muxer state is error! Need to be re-created!");
        need_break = true;
      }break;
      default:
        break;
    }
    if (AM_UNLIKELY(need_break)) {
      break;
    }
    AM_DESTROY(m_thread);
    if (AM_LIKELY(!m_thread)) {
      m_thread = AMThread::create(m_muxer_name.c_str(), thread_entry, this);
      if (AM_UNLIKELY(!m_thread)) {
        ERROR("Failed to create thread.");
        ret = AM_STATE_ERROR;
        break;
      }
    }
    while ((get_state() == AM_MUXER_CODEC_STOPPED)
        || (get_state() == AM_MUXER_CODEC_INIT)) {
      usleep(5000);
    }
    if(get_state() == AM_MUXER_CODEC_RUNNING) {
      NOTICE("Start %s success.", m_muxer_name.c_str());
    } else {
      ERROR("Failed to start %s.", m_muxer_name.c_str());
      ret = AM_STATE_ERROR;
      break;
    }
  } while (0);
  return ret;
}

AM_MUXER_CODEC_STATE AMMuxerJpegBase::create_resource()
{
  char file_name[strlen(m_muxer_jpeg_config->file_location.c_str())
        + strlen(m_muxer_jpeg_config->file_name_prefix.c_str()) + 128];
  memset(file_name, 0, sizeof(file_name));
  INFO("Begin to create resource in %s.", m_muxer_name.c_str());
  AM_MUXER_CODEC_STATE ret = AM_MUXER_CODEC_RUNNING;
  do {
    if (AM_UNLIKELY(generate_file_name(file_name) != AM_STATE_OK)) {
      ERROR("%s generate file name error, exit main loop.",
            m_muxer_name.c_str());
      ret = AM_MUXER_CODEC_ERROR;
      break;
    }
    AM_DESTROY(m_file_writer);
    if (AM_UNLIKELY((m_file_writer = AMJpegFileWriter::create (
        m_muxer_jpeg_config)) == NULL)) {
      ERROR("Failed to create m_file_writer in %s!", m_muxer_name.c_str());
      ret = AM_MUXER_CODEC_ERROR;
      break;
    }
    if (AM_UNLIKELY(m_file_writer->set_media_sink(file_name) != AM_STATE_OK)) {
      ERROR("Failed to set media sink for m_file_writer in %s!",
            m_muxer_name.c_str());
      ret = AM_MUXER_CODEC_ERROR;
      break;
    }
  } while (0);
  AUTO_SPIN_LOCK(m_state_lock);
  m_state = ret;
  return ret;
}

AM_STATE AMMuxerJpegBase::stop()
{
  AUTO_SPIN_LOCK(m_interface_lock);
  AM_STATE ret = AM_STATE_OK;
  m_run = false;
  AM_DESTROY(m_thread);
  NOTICE("Stop %s success.", m_muxer_name.c_str());
  return ret;
}

bool AMMuxerJpegBase::start_file_writing()
{
  bool ret = true;
  INFO("Begin to start file writing in %s.", m_muxer_name.c_str());
  char file_name[strlen(m_muxer_jpeg_config->file_location.c_str())
        + strlen(m_muxer_jpeg_config->file_name_prefix.c_str()) + 128];
  do{
    if(m_file_writing) {
      NOTICE("File writing is already startted in %s.", m_muxer_name.c_str());
      break;
    }
    if (m_file_writer) {
      if (AM_UNLIKELY(generate_file_name(file_name) != AM_STATE_OK)) {
        ERROR("Generate file name error in %s.", m_muxer_name.c_str());
        ret = false;
        break;
      }
      if (AM_UNLIKELY(m_file_writer->set_media_sink(file_name) != AM_STATE_OK)) {
        ERROR("Failed to set file name to m_file_writer in %s!",
              m_muxer_name.c_str());
        ret = false;
        break;
      }
    }
    m_file_writing = true;
    INFO("Start file writing success in %s.", m_muxer_name.c_str());
  }while(0);
  return ret;
}

bool AMMuxerJpegBase::stop_file_writing()
{
  bool ret = true;
  INFO("Begin to stop file writing in %s.", m_muxer_name.c_str());
  do{
    AUTO_SPIN_LOCK(m_file_writing_lock);
    if(!m_file_writing) {
      NOTICE("File writing is already stopped in %s", m_muxer_name.c_str());
      break;
    }
    m_file_writing = false;
    INFO("Stop file writing success in %s.", m_muxer_name.c_str());
  }while(0);
  return ret;
}

bool AMMuxerJpegBase::is_running()
{
  return m_run;
}

void AMMuxerJpegBase::release_resource()
{
  while (!(m_packet_queue->empty())) {
    m_packet_queue->front()->release();
    m_packet_queue->pop_front();
  }
  AM_DESTROY(m_file_writer);
  INFO("Release resource success in %s.", m_muxer_name.c_str());
}

uint8_t AMMuxerJpegBase::get_muxer_codec_stream_id()
{
  return (uint8_t)((0x01) << (m_stream_id));
}

uint32_t AMMuxerJpegBase::get_muxer_id()
{
  return m_muxer_jpeg_config->muxer_id;
}

AM_MUXER_CODEC_STATE AMMuxerJpegBase::get_state()
{
  AUTO_SPIN_LOCK(m_state_lock);
  return m_state;
}

AM_STATE AMMuxerJpegBase::set_config(AMMuxerCodecConfig* config)
{
  return AM_STATE_ERROR;
}

AM_STATE AMMuxerJpegBase::get_config(AMMuxerCodecConfig* config)
{
  return AM_STATE_ERROR;
}

void AMMuxerJpegBase::thread_entry(void* p)
{
  ((AMMuxerJpegBase*) p)->main_loop();
}

bool AMMuxerJpegBase::get_current_time_string(char *time_str, int32_t len)
{
  time_t current = time(NULL);
  if (AM_UNLIKELY(strftime(time_str, len, "%Y%m%d%H%M%S", localtime(&current))
                  == 0)) {
    ERROR("Date string format error!");
    time_str[0] = '\0';
    return false;
  }
  return true;
}

bool AMMuxerJpegBase::get_proper_file_location(std::string& file_location)
{
  bool ret = true;
  uint64_t max_free_space = 0;
  uint64_t free_space = 0;
  std::ifstream file;
  std::string read_line;
  std::string storage_str = "/storage";
  std::string sdcard_str = "/sdcard";
  string_list location_list;
  location_list.clear();
  size_t find_str_position = 0;
  INFO("Begin to get proper file location in %s.", m_muxer_name.c_str());
  do {
    if (m_muxer_jpeg_config->file_location_auto_parse) {
      std::string file_location_suffix;
      std::string::size_type pos = 0;
      if ((pos = m_muxer_jpeg_config->file_location.find(storage_str, pos))
          != std::string::npos) {
        pos += storage_str.size() + 1;
      } else {
        pos = 1;
      }
      pos = m_muxer_jpeg_config->file_location.find('/', pos);
      file_location_suffix = m_muxer_jpeg_config->file_location.substr(pos);
      file.open("/proc/self/mounts");
      INFO("mount information :");
      while (getline(file, read_line)) {
        std::string temp_location;
        temp_location.clear();
        INFO("%s", read_line.c_str());
        if ((find_str_position = read_line.find(storage_str))
            != std::string::npos) {
          for (uint32_t i = find_str_position;; ++ i) {
            if (read_line.substr(i, 1) != " ") {
              temp_location += read_line.substr(i, 1);
            } else {
              location_list.push_back(temp_location);
              INFO("Find a storage str : %s in %s", temp_location.c_str(),
                   m_muxer_name.c_str());
              break;
            }
          }
        } else if ((find_str_position = read_line.find(sdcard_str))
            != std::string::npos) {
          for (uint32_t i = find_str_position;; ++ i) {
            if (read_line.substr(i, 1) != " ") {
              temp_location += read_line.substr(i, 1);
            } else {
              location_list.push_back(temp_location);
              INFO("Find a sdcard str : %s in %s", temp_location.c_str(),
                   m_muxer_name.c_str());
              break;
            }
          }
        }
      }
      if (!location_list.empty()) {
        size_t find_str_pos_tmp = 0;
        string_list::iterator it = location_list.begin();
        for (; it != location_list.end(); ++ it) {
          if ((find_str_pos_tmp = m_muxer_jpeg_config->file_location.find(*it))
              == 0) {
            NOTICE("File location is on sdcard or usb storage in %s, great.",
                   m_muxer_name.c_str());
            file_location = m_muxer_jpeg_config->file_location;
            break;
          }
        }
        if (it == location_list.end()) {
          WARN("File location be set in config file is not on sdcard or"
              " usb storage, %s will auto parse a proper file location"
              "on sdcard or storage.", m_muxer_name.c_str());
          string_list::iterator max_free_space_it = location_list.begin();
          for (string_list::iterator i = location_list.begin();
              i != location_list.end(); ++ i) {
            struct statfs disk_statfs;
            if (statfs((*i).c_str(), &disk_statfs) < 0) {
              PERROR("File location statfs");
              ret = false;
              break;
            } else {
              free_space = ((uint64_t) disk_statfs.f_bsize
                  * (uint64_t) disk_statfs.f_bfree) / (uint64_t) (1024 * 1024);
              if (free_space > max_free_space) {
                max_free_space = free_space;
                max_free_space_it = i;
              }
            }
          }
          if (!ret) {
            break;
          }
          struct statfs disk_statfs;
          if (statfs(m_muxer_jpeg_config->file_location.c_str(), &disk_statfs)
              < 0) {
            WARN("File location in config file statfs error in %s.",
                 m_muxer_name.c_str());
            file_location = (*max_free_space_it) + file_location_suffix;
          } else {
            free_space = ((uint64_t) disk_statfs.f_bsize
                * (uint64_t) disk_statfs.f_bfree) / (uint64_t) (1024 * 1024);
            if (free_space > max_free_space) {
              NOTICE("The free space of file location in config file is larger"
                  "than sdcard free space in %s, use file location in "
                  "config file.", m_muxer_name.c_str());
              file_location = m_muxer_jpeg_config->file_location;
            } else {
              file_location = (*max_free_space_it) + file_location_suffix;
              NOTICE("The free space of file location in config file is smaller"
                  "than sdcard free space, set file location on sdcard in %s.",
                  m_muxer_name.c_str());
            }
          }
        } else {
          file_location = m_muxer_jpeg_config->file_location;
          break;
        }
      } else {
        NOTICE("Do not find storage or sdcard string in mount information in"
            " %s.", m_muxer_name.c_str());
        if (!AMFile::exists(m_muxer_jpeg_config->file_location.c_str())) {
          if (!AMFile::create_path(m_muxer_jpeg_config->file_location.c_str())) {
            ERROR("Failed to create file path: %s in %s!",
                  m_muxer_jpeg_config->file_location.c_str(),
                  m_muxer_name.c_str());
            ret = false;
            break;
          }
        }
        struct statfs disk_statfs;
        if (statfs(m_muxer_jpeg_config->file_location.c_str(), &disk_statfs)
            < 0) {
          PERROR("File location in config file statfs");
          ret = false;
          break;
        } else {
          free_space = ((uint64_t) disk_statfs.f_bsize
              * (uint64_t) disk_statfs.f_bfree) / (uint64_t) (1024 * 1024);
          if (free_space >= 20) {
            file_location = m_muxer_jpeg_config->file_location;
            NOTICE("Free space is larger than 20M, use it in %s.",
                   m_muxer_name.c_str());
            break;
          } else {
            ERROR("Free space is smaller than 20M, please"
                "set file location on sdcard or usb storage in %s.",
                m_muxer_name.c_str());
            ret = false;
            break;
          }
        }
      }
    } else { //file_location_auto_parse is false
      file_location = m_muxer_jpeg_config->file_location;
      break;
    }
    if (!ret) {
      break;
    }
    NOTICE("Get proper file location: %s success in %s.",
           file_location.c_str(), m_muxer_name.c_str());
  } while (0);
  if (file.is_open()) {
    file.close();
  }
  return ret;
}

void AMMuxerJpegBase::check_storage_free_space()
{
  uint64_t free_space = 0;
  struct statfs disk_statfs;
  if(statfs(m_file_location.c_str(), &disk_statfs) < 0) {
    PERROR("Statfs");
    ERROR("%s statfs error in %s", m_file_location.c_str(),
          m_muxer_name.c_str());
  } else {
    free_space = ((uint64_t)disk_statfs.f_bsize *
        (uint64_t)disk_statfs.f_bfree) / (uint64_t)(1024 * 1024);
    DEBUG("Free space is %llu M in %s", free_space, m_muxer_name.c_str());
    if(AM_UNLIKELY(free_space <=
                   m_muxer_jpeg_config->smallest_free_space)) {
      ERROR("The free space is smaller than %d M in %s, "
          "will stop writing data to jpeg file",
          m_muxer_jpeg_config->smallest_free_space, m_muxer_name.c_str());
      m_file_writing = false;
    }
  }
}

AM_STATE AMMuxerJpegBase::on_data_packet(AMPacket* packet)
{
  AM_STATE ret = AM_STATE_OK;
  do{
    if(packet->get_attr() == AMPacket::AM_PAYLOAD_ATTR_VIDEO) {
      if(AM_UNLIKELY(m_file_writer->write_data(packet->get_data_ptr(),
              packet->get_data_size()) != AM_STATE_OK)) {
        ERROR("Failed to write data to jpeg file.");
        ret = AM_STATE_ERROR;
        break;
      }
    } else {
      NOTICE("Jpeg muxer just support video stream.");
      ret = AM_STATE_ERROR;
      break;
    }
  }while(0);
  return ret;
}

AM_STATE AMMuxerJpegBase::on_eos_packet(AMPacket* packet)
{
  AM_STATE ret = AM_STATE_OK;
  m_run = false;
  NOTICE("Receive eos packet in %s, exit the main loop", m_muxer_name.c_str());
  return ret;
}

AM_STATE AMMuxerJpegBase::on_info_packet(AMPacket* packet)
{
  AM_STATE ret = AM_STATE_OK;
  NOTICE("Receive info packet in %s", m_muxer_name.c_str());
  do {
    AM_VIDEO_INFO* video_info = (AM_VIDEO_INFO*) (packet->get_data_ptr());
    if (!video_info) {
      ERROR("%s received video info is null", m_muxer_name.c_str());
      ret = AM_STATE_ERROR;
    }
    INFO("\n%s receive INFO:\n"
        "stream_id: %u\n"
        "     size: %d x %d\n"
        "        M: %d\n"
        "        N: %d\n"
        "     rate: %d\n"
        "    scale: %d\n"
        "      fps: %d\n"
        "      mul: %u\n"
        "      div: %u\n",
        m_muxer_name.c_str(),
        video_info->stream_id, video_info->width,
        video_info->height, video_info->M, video_info->N,
        video_info->rate, video_info->scale, video_info->fps,
        video_info->mul, video_info->div);
  } while(0);
  return ret;
}
