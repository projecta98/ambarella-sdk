/*******************************************************************************
 * am_rtp_data.h
 *
 * History:
 *   2015-1-6 - [ypchang] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/
#ifndef ORYX_STREAM_RECORD_MUXERS_MUXER_RTP_AM_RTP_DATA_H_
#define ORYX_STREAM_RECORD_MUXERS_MUXER_RTP_AM_RTP_DATA_H_

#include <atomic>
#include <mutex>

struct AMRtpPacket
{
    uint8_t *tcp_data;
    uint8_t *udp_data;
    uint32_t total_size;
    AMRtpPacket();
    uint8_t* tcp();
    uint32_t tcp_data_size();
    uint8_t* udp();
    uint32_t udp_data_size();
};

class AMIRtpSession;
struct AMRtpData
{
    int64_t         pts;
    uint8_t        *buffer;
    AMRtpPacket    *packet;
    AMIRtpSession  *owner;
    uint32_t        id;
    uint32_t        buffer_size;
    uint32_t        data_size;
    uint32_t        payload_size;
    uint32_t        pkt_size;
    uint32_t        pkt_num;
    std::atomic_int ref_count;
    AMRtpData();
    ~AMRtpData();
    void clear();
    bool create(AMIRtpSession *session, uint32_t datasize,
                uint32_t payloadsize,  uint32_t packet_num);
    void add_ref();
    void release();
};

#endif /* ORYX_STREAM_RECORD_MUXERS_MUXER_RTP_AM_RTP_DATA_H_ */
