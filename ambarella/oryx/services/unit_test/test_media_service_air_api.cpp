/*******************************************************************************
 * test_media_service_air_api.cpp
 *
 * History:
 *   2015-2-27 - [ccjing] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/
#include <signal.h>
#include <limits.h>
#include <iostream>
#include "am_base_include.h"
#include "am_log.h"
#include "am_define.h"
#include "getopt.h"

#include "am_api_helper.h"
#include "am_api_media.h"


static struct option long_options[] =
{
  {"help",                   no_argument,       0,  'h'},

  {"start_recording_engine", no_argument,       0,  'r' },
  {"stop_recording_engine",  no_argument,       0,  's' },
  {"add_audio_file",         required_argument, 0,  'a' },
  {"start_audio_playback",   no_argument,       0,  'y' },
  {"stop_audio_playback",    no_argument,       0,  'n' },
  {"pause_audio_playback",   no_argument,       0,  'p' },
  {"start_file_writing",     required_argument, 0,  'd' },
  {"stop_file_writing",      required_argument, 0,  'b' },
  {"send_event",             required_argument, 0,  'e' },
  {0,                        0,                 0,   0  }
};

static const char *short_options = "hrsa:ynpd:b:e:";

struct hint32_t_s {
    const char *arg;
    const char *str;
};

static const hint32_t_s hint32_t[] =
{
  { "",            "\t\t\t\t\t\t\t"   "Show usage\n" },

  { "",            "\t\t\t\t\t"   "Start recording engine." },
  { "",            "\t\t\t\t\t"   "Stop recording engine." },
  { "file name",   "\t\t\t\t"     "Add audio file to playback" },
  { "",            "\t\t\t\t\t"   "Start audio file playback." },
  { "",            "\t\t\t\t\t"   "Stop audio file playback." },
  { "",            "\t\t\t\t\t"   "Pause audio file playback." },
  { "muxer id",    "\t\t\t\t"     "Start file writing." },
  { "muxer id",    "\t\t\t\t"     "Stop file writing." },
  { "event_attr*event_id*pre_num*after_num*closest_num","Send event. event_attr: "
      "'h' menas h_264_or_h265_event, 'm' means mjpeg event. "
      "If you want to set closest_num, pre_num and after_num must be set to 0.\n" },
};

static void usage(int32_t argc, char **argv)
{
  printf("\n%s usage:\n\n", argv[0]);
  for (uint32_t i = 0; i < sizeof(long_options) / sizeof(long_options[0]) - 1;
      ++ i) {
    if (isalpha(long_options[i].val)) {
      printf("-%c,  ", long_options[i].val);
    } else {
      printf("    ");
    }
    printf("--%s", long_options[i].name);
    if (hint32_t[i].arg[0] != 0) {
      printf(" [%s]", hint32_t[i].arg);
    }
    printf("\t%s\n", hint32_t[i].str);
  }
  printf("Examples:\n  %s -r\n"
         "  %s -s\n"
         "  %s -a 1.aac -a 2.aac -y\n"
         "  %s -y\n"
         "  %s -n\n"
         "  %s -p\n"
         "  %s -d 7          NOTICE: 7 means start muxer0 muxer1 and muxer2 "
         "((1 << 0) | (1 << 1) | (1 << 2))\n"
         "  %s -b 6          NOTICE: 6 means stop muxer1 and muxer2 "
         "((1 << 1) | (1 << 2))\n"
         "  %s -e h*0        NOTICE: 'h' means h264 or h265 event, 0 means "
         "event_id is 0\n"
         "  %s -e m*1*2*3*0  NOTICE: 'm' means mjpeg event, 1 means "
         "stream1, 2 means before current pts number is 2, 3 means after "
         "current pts number is 3, 0 means closest current pts number is 0.\n"
         "  %s -e m*1*0*0*4  NOTICE: 4 means closest current pts number is 4.\n\n",
         argv[0], argv[0], argv[0], argv[0], argv[0], argv[0], argv[0],
         argv[0], argv[0], argv[0], argv[0]);
}

bool parse_event_arg(std::string event_arg, AMIApiMediaEvent& event);

int main(int argc, char **argv)
{
  int ret = 0;
  int result = 0;//method call result
  AMIApiPlaybackAudioFileList* audio_file = nullptr;
  AMIApiMediaEvent* event = nullptr;
  do {
    audio_file = AMIApiPlaybackAudioFileList::create();
    if(!audio_file) {
      ERROR("Failed to create AMIApiPlaybackAudioFileList");
      ret = -1;
      break;
    }
    if (argc < 2) {
      usage(argc, argv);
      ret = -1;
      break;
    }
    AMAPIHelperPtr g_api_helper = NULL;
    if ((g_api_helper = AMAPIHelper::get_instance()) == NULL) {
      ERROR("CCWAPIConnection::get_instance failed.\n");
      ret = -1;
      break;
    }
    int opt = 0;
    while((opt = getopt_long(argc, argv, short_options, long_options,
                             nullptr)) != -1) {
      switch (opt) {
        case 'h' : {
          usage(argc, argv);
        } break;
        case 'r' : {
          g_api_helper->method_call(AM_IPC_MW_CMD_MEDIA_START_RECORDING,
                                    nullptr, 0, &result, sizeof(int));
          if (result < 0) {
            ERROR("Failed to start recording engine!");
            ret = -1;
            break;
          }
        } break;
        case 's' : {
          g_api_helper->method_call(AM_IPC_MW_CMD_MEDIA_STOP_RECORDING,
                                    nullptr, 0, &result, sizeof(int));
          if (result < 0) {
            ERROR("Failed to stop recording!");
            ret = -1;
            break;
          }
        } break;
        case 'a' : {
          std::string file_name = optarg;
          char abs_path[PATH_MAX] = { 0 };
          if ((NULL != realpath(file_name.c_str(), abs_path))
              && !audio_file->add_file(std::string(abs_path))) {
            ERROR("Failed to add file %s to file list, "
                  "file name maybe too long, drop it.");
          }
          g_api_helper->method_call(AM_IPC_MW_CMD_MEDIA_ADD_AUDIO_FILE,
                                    audio_file->get_file_list(),
                                    audio_file->get_file_list_size(),
                                    &result,
                                    sizeof(int));
          if (result < 0) {
            ERROR("Failed to add audio file.");
            ret = -1;
          }
          audio_file->clear_file();
        } break;
        case 'y' : {
          g_api_helper->method_call(
          AM_IPC_MW_CMD_MEDIA_START_PLAYBACK_AUDIO_FILE,
                                    NULL, 0, &result, sizeof(int));
          if (result < 0) {
            ERROR("Failed to start playback audio file.");
            ret = -1;
            break;
          }
        } break;
        case 'n' : {
          g_api_helper->method_call(AM_IPC_MW_CMD_MEDIA_STOP_PLAYBACK_AUDIO_FILE,
                                    NULL, 0, &result, sizeof(int));
          if(result < 0) {
            ERROR("Failed to stop playback audio file.");
            ret = -1;
            break;;
          }
        } break;
        case 'p' : {
          g_api_helper->method_call(AM_IPC_MW_CMD_MEDIA_PAUSE_PLAYBACK_AUDIO_FILE,
                                    NULL, 0, &result, sizeof(int));
          if (result < 0) {
            ERROR("Failed to pause playback audio file.");
            ret = -1;
            break;;
          }
        } break;
        case 'd' : {
          uint32_t muxer_id = atoi(optarg);
          g_api_helper->method_call(AM_IPC_MW_CMD_MEDIA_START_FILE_RECORDING,
                             &muxer_id, sizeof(muxer_id), &result, sizeof(int));
          if (result < 0) {
            ERROR("Failed to start file recording!");
            ret = -1;
            break;
          }
        } break;
        case 'b' : {
          uint32_t muxer_id = atoi(optarg);
          g_api_helper->method_call(AM_IPC_MW_CMD_MEDIA_STOP_FILE_RECORDING,
                             &muxer_id, sizeof(muxer_id), &result, sizeof(int));
          if (result < 0) {
            ERROR("Failed to stop file recording!");
            ret = -1;
            break;
          }
        } break;
        case 'e' : {
          std::string event_arg = optarg;
          event = AMIApiMediaEvent::create();
          if (!event) {
            ERROR("Failed to create AMIApiMediaEventStruct");
            ret = -1;
            break;
          }
          if (!parse_event_arg(event_arg, *event)) {
            ERROR("Failed to parse event arg.");
            ret = -1;
            break;
          }
          g_api_helper->method_call(AM_IPC_MW_CMD_MEDIA_EVENT_RECORDING_START,
                                    event->get_data(), event->get_data_size(),
                                    &result, sizeof(int));
          if (result < 0) {
            ERROR("Failed to start event recording!");
            ret = -1;
            break;
          }
        } break;
        default : {
          ERROR("Invalid short_options");
          ret = -1;
        } break;
      }
    }
    if (ret < 0) {
      break;
    }
  } while (0);
  delete audio_file;
  delete event;
  return ret;
}

bool parse_event_arg(std::string event_arg, AMIApiMediaEvent& event)
{
  bool ret = true;
  do {
    std::string find_flag = "*";
    std::string::size_type pos = 0;
    switch (event_arg[0]) {
      case 'h' : {
        event.set_attr_h26X();
        if (!event.set_event_id(atoi(&(event_arg[2])))) {
          ERROR("Failed to set event id.");
          ret = false;
        }
        INFO("receive 264 video event, event id is %u", atoi(&(event_arg[2])));
      } break;
      case 'm' : {
        event.set_attr_mjpeg();
        if (!event.set_event_id(atoi(&(event_arg[2])))) {
          ERROR("Failed to set event id.");
          ret = false;
          break;
        }
        pos = 2;
        if ((pos = event_arg.find(find_flag, pos)) != std::string::npos) {
          pos += 1;
          if (!event.set_pre_cur_pts_num(atoi(&(event_arg[pos])))) {
            ERROR("Failed to set pre cur pts num");
            ret = false;
            break;
          }
        } else {
          ERROR("Mjpeg event param is invalid.");
          ret = false;
          break;
        }
        if ((pos = event_arg.find(find_flag, pos)) != std::string::npos) {
          pos += 1;
          if (!event.set_after_cur_pts_num(atoi(&(event_arg[pos])))) {
            ERROR("Failed to set after cur pts num");
            ret = false;
            break;
          }
        } else {
          ERROR("Mjpeg event param is invalid.");
          ret = false;
          break;
        }
        if ((pos = event_arg.find(find_flag, pos)) != std::string::npos) {
          pos += 1;
          if (!event.set_closest_cur_pts_num(atoi(&(event_arg[pos])))) {
            ERROR("Failed to set closest cur pts num");
            ret = false;
            break;
          }
        } else {
          ERROR("Mjpeg event param is invalid.");
          ret = false;
          break;
        }
        INFO("Receive mjpeg event, event id is %u, pre num is %u,"
            "after num is %u, closest num is %u", event.get_event_id(),
            event.get_pre_cur_pts_num(), event.get_after_cur_pts_num(),
            event.get_closest_cur_pts_num());
      } break;
      default : {
        ERROR("Event attr error : %c", event_arg[0]);
        ret = false;
        break;
      }
    }
  } while(0);
  return ret;
}

